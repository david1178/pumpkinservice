# Pumpkin Self-hosting service 南瓜自有主機的服務 （陸續增加中..）

## index

![](https://i.imgur.com/lQ9NZ2j.png)


| Service | URL | Description |
| -------- | -------- | -------- |
| **IＤ**  | [Portal ](http://office.pumpkinvrar.com:5000)    | 更改密碼 > []因安全性考量僅能在公司7F環境中更改密碼   |
| **Email**     | [EMail email ](http://office.pumpkinvrar.com/mail)    | 容量無上限   |
| **Note**     | [Note ](http://office.pumpkinvrar.com/note)    | 知識庫KM     |
| **WIFI**    |  Pumpkin VR AR  |  個人化WIFI   |
| **VPN**    | [操作說明 ReadMe](https://hackmd.io/L_BTkfOhTDWNF1WUdcjaVg?view#Pumpkin-VPN)   |  連線入公司網路環境     |


```
> 以上說明
> 如果使用上有問題 請直接跟DAVID聯繫  telegram @a7a8a9abc
```
![](https://i.imgur.com/y20oor4.png)

---
## FAQ 
Email login 
不用輸入 @office.pumpkinvrar.com
只要輸入帳號即可

![](https://i.imgur.com/iL7FhSB.jpg)


---
## Pumpkin VPN

![](https://i.imgur.com/SXxzgIs.png)

![](https://i.imgur.com/63zC0nH.png)


### VPN伺服器位址 Host Address
> vpn.office.pumpkinvrar.com

### 共用金鑰 VPN Shared key
> pumpkin12345

### 使用者名稱/密碼  id / password
> the same with your Email 
http://office.pumpkinvrar.com/mail/


---
### 更改密碼 To change my password
http://office.pumpkinvrar.com:5000/
![](https://i.imgur.com/e0XWXos.jpg)
![](https://i.imgur.com/rDR1Aq1.png)


---

## 如果忘記密碼的話 可以重設  forget my password and reset it
![](https://i.imgur.com/kOAeReN.png)


---

WIFI Andorid 

![](https://i.imgur.com/bJ0PDqL.png)
